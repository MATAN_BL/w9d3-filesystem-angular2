import { W9d3FileSystemAngular2Page } from './app.po';

describe('w9d3-file-system-angular2 App', function() {
  let page: W9d3FileSystemAngular2Page;

  beforeEach(() => {
    page = new W9d3FileSystemAngular2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
