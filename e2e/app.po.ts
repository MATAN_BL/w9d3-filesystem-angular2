import { browser, element, by } from 'protractor';

export class W9d3FileSystemAngular2Page {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}
