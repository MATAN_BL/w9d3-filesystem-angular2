import {Component, OnInit, Input} from '@angular/core';
import {LogicFile} from "../LogicEntity/LogicFile";

@Component({
  selector: 'content-file',
  template: `<input readonly [value] = "logicFile.name" cols = "logicFolder.name.length">`,
  styles: [`
    input {
        display: flex;
        background-color: inherit;
        border: none;
        width:110%;
        /*justify-content: center;*/
      }
  `]
})
export class ContentFileComponent implements OnInit {
  @Input() name: string;
  @Input() logicFile: LogicFile;

  constructor() {

  }

  ngOnInit() {
  }

}
