import {Component, OnInit, Input, ViewEncapsulation} from '@angular/core';
import {LogicFolder} from "../LogicEntity/LogicFolder";

@Component({
  selector: 'content-folder',
  template: `<input readonly [value] = "logicFolder.name">`,
  styles: [`
    input {
        border: none;
        background-color: inherit;
        width: 110%;
        justify-content: center;
      }
  `]
})
export class ContentFolderComponent implements OnInit {
  @Input() name: string;
  @Input() logicFolder: LogicFolder;
  @Input() id: number;

  constructor() {
  }
  ngOnInit() {
    this.name = name;
  }


}
