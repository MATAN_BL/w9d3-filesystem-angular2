import { Component, OnInit } from '@angular/core';
import {MainService} from "../app/service";

@Component({
  selector: 'content',
  templateUrl: 'content.component.html',
  styleUrls: ['content.styles.scss']
})
export class ContentComponent implements OnInit {

  constructor(private mainService: MainService) {}

  ngOnInit() {

  }

  rightClick() {
    console.log("***** RIGHT CLICK ****");
  }

}
