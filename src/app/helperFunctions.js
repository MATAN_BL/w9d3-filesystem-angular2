"use strict";
function getSubObjByPath(path, obj) {
    path = ((path.charAt(path.length - 1) == '/') ? (path.slice(0, path.length - 1)) : path);
    var pathArr = path.split('/');
    if ((pathArr[0] != "") || (pathArr[1] != 'root')) {
        return null;
    }
    var index = 2;
    while (index < pathArr.length) {
        obj = getChildObjOfName(obj, pathArr[index]);
        if (obj == null) {
            return null;
        }
        index++;
    }
    return obj;
    function getChildObjOfName(obj, name) {
        if (obj.children == null) {
            return null;
        }
        var resObj = null;
        obj.children.forEach(function (child) {
            if (child.name == name) {
                resObj = child;
            }
        });
        return resObj;
    }
}
exports.getSubObjByPath = getSubObjByPath;
function getParentObj(obj, childId) {
    if (obj == null || obj.children == null) {
        return null;
    }
    for (var i = 0; i < obj.children.length; i++) {
        if (obj.children[i].id == childId) {
            return obj;
        }
        else {
            var retObj = getParentObj(obj.children[i], childId);
            if (retObj != null) {
                return retObj;
            }
        }
    }
    return null;
}
exports.getParentObj = getParentObj;
function getSubObjById(obj, id) {
    if (obj.id == id) {
        return obj;
    }
    if (obj.children) {
        for (var i = 0; i < obj.children.length; i++) {
            var retObj = getSubObjById(obj.children[i], id);
            if (retObj) {
                return retObj;
            }
        }
    }
    else {
        return null;
    }
}
exports.getSubObjById = getSubObjById;
function checkUniqueness(name, parentFolder) {
    for (var i = 0; i < parentFolder.children.length; i++) {
        if (parentFolder.children[i].name == name) {
            return false;
        }
    }
    return true;
}
exports.checkUniqueness = checkUniqueness;
