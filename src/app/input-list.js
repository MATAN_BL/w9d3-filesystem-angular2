"use strict";
var LogicFolder_1 = require("../LogicEntity/LogicFolder");
var LogicFile_1 = require("../LogicEntity/LogicFile");
exports.inputList = new LogicFolder_1.LogicFolder(0, 'root');
var id1 = new LogicFile_1.LogicFile(1, 'Abraham', 'Abraham_text');
exports.inputList.addChild(id1);
var id2 = new LogicFile_1.LogicFile(2, 'David', 'David_text');
exports.inputList.addChild(id2);
var id3 = new LogicFile_1.LogicFile(3, 'Sara', 'SaraText');
exports.inputList.addChild(id3);
var id11 = new LogicFile_1.LogicFile(11, 'Benjamine', 'BennyText');
exports.inputList.addChild(id11);
var id4 = new LogicFolder_1.LogicFolder(4, 'dir1');
exports.inputList.addChild(id4);
var id5 = new LogicFile_1.LogicFile(5, 'Aviv', 'Aviv file');
id4.addChild(id5);
var id6 = new LogicFolder_1.LogicFolder(6, 'dir2');
id4.addChild(id6);
var id7 = new LogicFile_1.LogicFile(7, 'Gefen', 'Gefen File');
id6.addChild(id7);
var id8 = new LogicFolder_1.LogicFolder(8, 'dir3');
id6.addChild(id8);
var id9 = new LogicFile_1.LogicFile(9, 'Shlomi', 'shlomi file');
id8.addChild(id9);
exports.rootId = 0;
//   {
//   name: 'root', id: 0, type: 'folder', children: [
//     {name: 'dir1', id: 1, type: 'folder', children: []},
//     {name: 'dir2', id: 2, type: 'folder', children:[
//       {name: 'David', id:4, type: 'file', content: "This is David File"}
//     ]},
//     {name: 'Michal', id: 3, type: 'file', content: "This is Michal's TreeFile"}
//   ]
// };
