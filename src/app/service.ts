import { Injectable } from '@angular/core';
import {inputList} from './input-list';
import {LogicEntity} from "../LogicEntity/LogicEntity";
import {LogicFolder} from "../LogicEntity/LogicFolder";
import * as helper from "./helperFunctions";
import {RightClickService} from "./right-click/right-click.service";

@Injectable()
export class MainService {
  private _root: LogicFolder = inputList;
  private _curPath: string = "/root";
  private _curEntity: LogicEntity = inputList;
  private _showFileContent: boolean = false;
  private _hideContextMenu: boolean = false;
  private _topContextMenu: string;
  private _leftContextMenu: string;
  private _historyList: number[] = [this.root.id];
  private _curIndex: number = 0;

  constructor() {
    console.log("Constructor is init");
  }

  goBack() {
    while (this.curIndex > 0) {
      this.curIndex -= 1;
      let newEntity = helper.getSubObjById(this.root, this.historyList[this.curIndex]);
      this.updateCurEntity(newEntity, false);
      break;
    }
    console.log(this.historyList);
    console.log(this.curIndex);
  }
  goForward() {
    while (this.curIndex < this.historyList.length - 1) {
      this.curIndex += 1;
      let newEntity = helper.getSubObjById(this.root, this.historyList[this.curIndex]);
      this.updateCurEntity(newEntity, false);
      break;
    }
    console.log(this.historyList);
    console.log(this.curIndex);
  }

  addToHistory(id) {
    this.historyList.push(id);
    this.curIndex = this.historyList.length - 1;
    console.log(this.historyList);
    console.log(this.curIndex);
  }

  cleanHistory(idToDelete: number) {

    let offsprings = getAllOffsprings(idToDelete, this.root);
    for (let i = 0; i < this.historyList.length; i++) {
      if (offsprings.indexOf(this.historyList[i]) != -1) {
        if (i <= this.curIndex) {
          this.curIndex--;
        }
        this.historyList.splice(i, 1);
        i--;
      }
    }

    function getAllOffsprings(id: number, root: LogicFolder) {
      let obj = helper.getSubObjById(root, id);
      let offsprings = [];
      DFS(obj);
      return offsprings;

      function DFS(obj) {
        offsprings.push(obj.id);
        if (obj.getType() == 'folder') {
          obj.children.forEach(function(child) {
            DFS(child);
          })
        }
      }
    }
  }

  renamePath(input: any) {
    let oldPath = this.curPath;
    console.log("oldPath = " + oldPath);
    let newPath = input.value;
    let resObj = helper.getSubObjByPath(newPath, this._root);
    if (resObj != null) {
      this.curEntity = resObj;
      this.curPath = newPath;
      // ui.history.addToHistory(resObj.id);
      // ui.refreshEntityContentElem(ui.history);
    } else {
      input.value = oldPath;
    }
  }

  updateCurEntity(entity: LogicEntity, addToHistory: boolean) {
    var newId = entity.id;
    if (addToHistory) {
      this.addToHistory(newId);
      this.curIndex = this.historyList.length - 1;
    }
    console.log("************* UPDATE_CUR_ENTITY", newId);

    let result = getFolderRecur.call(this, this._root, newId, "/root");
      this._curEntity = result;

      function getFolderRecur(this: any, obj: any, id: number, path: string):any {
          if (obj.id == id) {
              this._curPath = path;
              return obj;
          }
          if (obj.children == null) { return null }
          for (let i = 0; i < obj.children.length; i++) {
              var newPath = `${path}/${obj.children[i].name}`;
              var result = getFolderRecur.call(this, obj.children[i], id, newPath);
              if (result!=null) {
                  return result;
              }
          }
          return null;
      }
  }

  setLocationOfContextMenu(left: string, top: string) {
    // this.hideContextMenu = false;
    this._leftContextMenu = `${left.toString()}px`;
    this._topContextMenu = `${top.toString()}px`;
  }


  get root(): LogicFolder {
    return this._root;
  }

  set root(value: LogicFolder) {
    this._root = value;
  }

  get curPath(): string {
    return this._curPath;
  }

  set curPath(value: string) {
    this._curPath = value;
  }

  get curEntity(): LogicEntity {
    return this._curEntity;
  }

  set curEntity(value: LogicEntity) {
    this._curEntity = value;
  }

  get showFileContent(): boolean {
    return this._showFileContent;
  }

  set showFileContent(value: boolean) {
    this._showFileContent = value;
  }


  get hideContextMenu(): boolean {
    return this._hideContextMenu;
  }

  set hideContextMenu(value: boolean) {
    this._hideContextMenu = value;
  }

  get topContextMenu(): string {
    return this._topContextMenu;
  }

  set topContextMenu(value: string) {
    this._topContextMenu = value;
  }

  get leftContextMenu(): string {
    return this._leftContextMenu;
  }

  set leftContextMenu(value: string) {
    this._leftContextMenu = value;
  }

  get historyList(): number[] {
    return this._historyList;
  }

  set historyList(value: number[]) {
    this._historyList = value;
  }

  get curIndex(): number {
    return this._curIndex;
  }

  set curIndex(value: number) {
    this._curIndex = value;
  }


}
