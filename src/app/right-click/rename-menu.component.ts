import { Component, OnInit } from '@angular/core';
import {RightClickService} from "./right-click.service";

@Component({
  selector: 'rename-menu',
  template: `
    <menu type="context" data-menu-name = "renameMenu" *ngIf = "!rightClickService.hideRenameMenu"
        [style.top] = "rightClickService.topContextMenu" [style.left] = "rightClickService.leftContextMenu">
      <div [ngSwitch]="rightClickService.newEntityMode">
        <p *ngSwitchCase = "'none'"> Please Type a new name: </p>
        <p *ngSwitchCase = "'newfile'"> Please give a name to the File: </p>
        <p *ngSwitchCase = "'newfolder'"> Please give a name to the Folder: </p>
      </div>

      <input [value] = "rightClickService.entityClicked.name" autofocus #newNameInput><br>
      <button (click) = "rightClickService.takeNewName(newNameInput, $event)">Save</button>
      <div [ngSwitch]="rightClickService.errorCode">
        <h3 class="error" *ngSwitchCase = "1"> Illegal Name!</h3>
        <h3 class="error" *ngSwitchCase = "2"> New name is not unique in the path</h3>
      </div>
    </menu>
  `,
  styleUrls: ['./right-click.component.css']
})

export class RenameMenuComponent implements OnInit {

  constructor(private rightClickService: RightClickService) { }

  ngOnInit() {
  }

}
