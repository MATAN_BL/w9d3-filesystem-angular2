import { Component, OnInit } from '@angular/core';
import {RightClickService} from "./right-click.service";

@Component({
  selector: 'right-click',
  template: `
    <context-menu *ngIf = "!rightClickService.hideContextMenu"></context-menu>
    <rename-menu *ngIf = "!rightClickService.hideRenameMenu"></rename-menu>
  `,
  styleUrls: ['right-click.component.scss'],
})
export class RightClickComponent implements OnInit {

  hideContextMenu: boolean = false;
  hideRenameMenu: boolean = false;

  constructor(private rightClickService: RightClickService) {}

  ngOnInit() {

  }

}
