import { Injectable } from '@angular/core';
import {LogicEntity} from "../../LogicEntity/LogicEntity";
import {LogicFolder} from "../../LogicEntity/LogicFolder";
import {inputList} from "../input-list";
import * as helper from '../helperFunctions';
import {MainService} from "../service";
import {LogicFile} from "../../LogicEntity/LogicFile";

@Injectable()
export class RightClickService {
  private _hideContextMenu: boolean = true;
  private _entityClicked: any;
  private _hideRenameMenu: boolean = true;
  private _root: LogicFolder = inputList;
  private _isItAFolder: boolean;
  private _maxId: number;
  private _folderClicked: LogicFolder;
  private _newEntityMode: string = "none";  // can be "none", "newfile" or "newfolder"
  private _errorCode: number;  // 0 - OK, 1 - empty, 2 - not unique


  constructor(private _mainService: MainService) {
    this.maxId = this.getMaxId(this.root);
  }

  getMaxId(rootFolder: LogicFolder) {
    if (rootFolder.children == [] || rootFolder.children == undefined) {
      return rootFolder.id;
    }
    let options = [rootFolder.id];
    for (let i = 0; i < rootFolder.children.length; i++) {
      options.push(this.getMaxId(rootFolder.children[i]));
    }
    return options.sort(function (a, b) {
      return b - a
    })[0];
  }

  deleteItem() {
    let id: number = this.entityClicked.id;
    if (id == this.root.id) {
      return false;
    }
    let parentObj = helper.getParentObj(this.root, id);
    this._mainService.cleanHistory(id);
    this.hideContextMenu = true;
    return parentObj.deleteChild(id);
  }

  renameItem() {
    this.hideContextMenu = true;
    this.hideRenameMenu = false;
    console.log(this.entityClicked);
  }

  takeNewName(newNameInput: any, $event: any) {
    $event.stopPropagation();
    console.log(newNameInput.value);
    let oldName = this.entityClicked.name;
    let parentFolder = helper.getParentObj(this.root, this.entityClicked.id);
    let newName = newNameInput.value;
    if (newName == "") {
      this.errorCode = 1;
    }
    else if (!helper.checkUniqueness(newName, oldName, parentFolder)) {
      this.errorCode = 2;
    }
    else {
      this.entityClicked.name = newName;
      this.hideRenameMenu = true;
      this.errorCode = 0;
      this.newEntityMode = "none";
    }
  }

  addFile() {
    this.maxId++;
    this.newEntityMode = "newfile";
    let newFile = new LogicFile(this._maxId, "", "" );
    this.folderClicked.addChild(newFile);
    this.entityClicked = newFile;
    this.renameItem();
  }

  addFolder() {
    this.maxId++;
    this.newEntityMode = "newfolder";
    let newFolder = new LogicFolder(this._maxId, "");
    this.folderClicked.addChild(newFolder);
    this.entityClicked = newFolder;
    this.renameItem();
  }

  get hideContextMenu(): boolean {
    return this._hideContextMenu;
  }

  set hideContextMenu(value: boolean) {
    this._hideContextMenu = value;
  }

  get entityClicked(): LogicEntity {
    return this._entityClicked;
  }

  set entityClicked(value: LogicEntity) {
    this._entityClicked = value;
  }

  get hideRenameMenu(): boolean {
    return this._hideRenameMenu;
  }

  set hideRenameMenu(value: boolean) {
    this._hideRenameMenu = value;
  }

  get root(): LogicFolder {
    return this._root;
  }

  set root(value: LogicFolder) {
    this._root = value;
  }


  get isItAFolder(): boolean {
    return this._isItAFolder;
  }

  set isItAFolder(value: boolean) {
    this._isItAFolder = value;
  }

  get maxId(): number {
    return this._maxId;
  }

  set maxId(value: number) {
    this._maxId = value;
  }

  get folderClicked(): LogicFolder {
    return this._folderClicked;
  }

  set folderClicked(value: LogicFolder) {
    this._folderClicked = value;
  }


  get newEntityMode(): string {
    return this._newEntityMode;
  }

  set newEntityMode(value: string) {
    this._newEntityMode = value;
  }

  get errorCode(): number {
    return this._errorCode;
  }

  set errorCode(value: number) {
    this._errorCode = value;
  }

  get mainService(): MainService {
    return this._mainService;
  }

  set mainService(value: MainService) {
    this._mainService = value;
  }
}
