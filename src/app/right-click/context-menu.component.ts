import { Component, OnInit } from '@angular/core';
import {RightClickService} from "./right-click.service";

@Component({
  selector: 'context-menu',
  template: `
    <menu type="context" data-menu-name = "rightClickMenu">
      <menuitem id="newFolderButton" (click) = "rightClickService.addFolder()" *ngIf = "rightClickService.isItAFolder">
        New Folder</menuitem>
      <menuitem id="newFileButton" (click) = "rightClickService.addFile()" *ngIf = "rightClickService.isItAFolder"> 
        New File </menuitem>
      <menuitem id="deleteButton" (click) = "rightClickService.deleteItem(); false">Delete</menuitem>
      <menuitem id="renameButton" (click) = "rightClickService.renameItem($event); false">Rename</menuitem>
    </menu>
  `,
  styleUrls: ['./right-click.component.css']
})
export class ContextMenuComponent implements OnInit {

  constructor(private rightClickService: RightClickService) { }

  ngOnInit() {
  }

}
