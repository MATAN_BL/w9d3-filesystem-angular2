"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var input_list_1 = require("../input-list");
var helper = require('../helperFunctions');
var RightClickService = (function () {
    function RightClickService() {
        this._hideContextMenu = true;
        this._hideRenameMenu = true;
        this._root = input_list_1.inputList;
    }
    RightClickService.prototype.deleteItem = function () {
        var id = this.entityClicked.id;
        if (id == this.root.id) {
            return false;
        }
        var parentObj = helper.getParentObj(this.root, id);
        return parentObj.deleteChild(id);
    };
    RightClickService.prototype.setLocationOfContextMenu = function (left, top) {
        // this.hideContextMenu = false;
        this.leftContextMenu = left.toString() + "px";
        this.topContextMenu = top.toString() + "px";
    };
    RightClickService.prototype.renameItem = function ($event) {
        this.hideContextMenu = true;
        // let itemObj = this.entityClicked;
        console.log(this.entityClicked);
        this.hideRenameMenu = false;
    };
    RightClickService.prototype.takeNewName = function (newNameInput, $event) {
        console.log(newNameInput.value);
        var parentFolder = helper.getParentObj(this.root, this.entityClicked.id);
        var newName = newNameInput.value;
        if (helper.checkUniqueness(newName, parentFolder) == true) {
            this.entityClicked.name = newName;
            console.log("here");
            this.hideRenameMenu = true;
        }
        else {
            document.getElementById('error').style.display = 'block';
        }
        $event.stopPropagation();
    };
    Object.defineProperty(RightClickService.prototype, "hideContextMenu", {
        get: function () {
            return this._hideContextMenu;
        },
        set: function (value) {
            this._hideContextMenu = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RightClickService.prototype, "topContextMenu", {
        get: function () {
            return this._topContextMenu;
        },
        set: function (value) {
            this._topContextMenu = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RightClickService.prototype, "leftContextMenu", {
        get: function () {
            return this._leftContextMenu;
        },
        set: function (value) {
            this._leftContextMenu = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RightClickService.prototype, "entityClicked", {
        get: function () {
            return this._entityClicked;
        },
        set: function (value) {
            this._entityClicked = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RightClickService.prototype, "hideRenameMenu", {
        get: function () {
            return this._hideRenameMenu;
        },
        set: function (value) {
            this._hideRenameMenu = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RightClickService.prototype, "root", {
        get: function () {
            return this._root;
        },
        set: function (value) {
            this._root = value;
        },
        enumerable: true,
        configurable: true
    });
    RightClickService = __decorate([
        core_1.Injectable()
    ], RightClickService);
    return RightClickService;
}());
exports.RightClickService = RightClickService;
