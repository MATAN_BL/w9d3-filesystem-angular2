import {Directive, ElementRef, Input, OnInit} from '@angular/core';
import {MainService} from "./service";
import {LogicFile} from "../LogicEntity/LogicFile";
import {LogicFolder} from "../LogicEntity/LogicFolder";
import * as helper from './helperFunctions';
import {RightClickService} from "./right-click/right-click.service";
import {LogicEntity} from "../LogicEntity/LogicEntity";

@Directive({
  selector: '[entityClick]',
  host: {
    '(click)': 'updateCurEntity()',
    '(contextmenu)': 'rightClick($event)',
    '(blur)':'rename()'
  }
})
export class EntityClickDirective implements OnInit{
  @Input() logicFile: LogicFile;
  @Input() logicFolder: LogicFolder;
  entity: LogicEntity;

  constructor(private mainService: MainService, private rightClickService: RightClickService, private elementRef: ElementRef) {
  }

  ngOnInit() {
    this.entity = this.logicFile || this.logicFolder;
  }

  rename() {
    let parentObj = helper.getParentObj(this.mainService.root, this.entity);
    console.log(this.entity.name);

    // return itemObj.rename(newName, parentObj)
  }

  isItAFile(): boolean {
    if (this.logicFile) {
      return true;
    } else {
      return false;
    }
  }

  updateCurEntity() {
    if (this.isItAFile()) {
      console.log("A file was clicked");
      this.mainService.updateCurEntity(this.logicFile, true);
    } else {
      console.log("A folder was clicked");
      this.mainService.updateCurEntity(this.logicFolder, true);
    }
  }

  rightClick($event) {
    this.rightClickService.entityClicked = this.logicFile || this.logicFolder;
    this.rightClickService.folderClicked = this.logicFolder;
    if (this.rightClickService.entityClicked.getType() == 'folder') {
      this.rightClickService.isItAFolder = true;
    } else {
      this.rightClickService.isItAFolder = false;
    }
    this.rightClickService.hideContextMenu = false;
    this.mainService.setLocationOfContextMenu($event.pageX, $event.pageY);
    console.log(`pageX: ${$event.pageX}; pageY: ${$event.pageY}`);
    return false; // stop propagation
  }

}
