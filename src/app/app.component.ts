import { Component } from '@angular/core';
import { MainService } from "./service";
import {RightClickService} from "./right-click/right-click.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  title = 'app works!';

  constructor(private rightClickService: RightClickService) {
    // var service = this.mainService;
    document.body.addEventListener('click', () => {
      rightClickService.mainService.hideContextMenu = true;
    })
  }
}
