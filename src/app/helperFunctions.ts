import {LogicEntity} from "../LogicEntity/LogicEntity";
import {LogicFolder} from "../LogicEntity/LogicFolder";

export function getSubObjByPath(path: string, obj: LogicFolder): LogicEntity {

  path = ( (path.charAt(path.length - 1) == '/') ? (path.slice(0, path.length - 1)) : path);
  let pathArr = path.split('/');

  if ((pathArr[0] != "") || (pathArr[1] != 'root')) {
    return null;
  }
  let index = 2;
  while (index < pathArr.length) {
    obj = getChildObjOfName(obj, pathArr[index]);
    if (obj == null) {
      return null;
    }
    index++;
  }
  return obj;

  function getChildObjOfName(obj, name) {
    if (obj.children == null) {
      return null;
    }
    let resObj = null;
    obj.children.forEach(function (child) {
      if (child.name == name) {
        resObj = child;
      }
    });
    return resObj;
  }

}

export function getParentObj(obj, childId) {
  if (obj == null || obj.children == null) {
    return null;
  }
  for (let i = 0; i < obj.children.length; i++) {
    if (obj.children[i].id == childId) {
      return obj;
    } else {
      let retObj = getParentObj(obj.children[i], childId);
      if (retObj != null) {
        return retObj;
      }
    }
  }
  return null;
}

export function getSubObjById(root, id) {
  if (root.id == id) {
    return root;
  }
  if (root.children) {
    for (let i = 0; i < root.children.length; i++) {
      let retObj = getSubObjById(root.children[i], id);
      if (retObj) {
        return retObj;
      }
    }
  } else {
    return null;
  }
}

export function checkUniqueness(newName: string, oldName: string, parentFolder: LogicFolder) {
  for (let i = 0; i < parentFolder.children.length; i++) {
    if ((parentFolder.children[i].name == newName) && (oldName != newName)) {
      return false;
    }
  }
  return true;
}
