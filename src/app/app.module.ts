import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {MainService} from './service';
import { TreeFolder } from '../Tree/TreeFolder/TreeFolder.component';
import { TreeFile } from '../Tree/TreeFile/TreeFile.component';
import { ContentComponent } from '../content/content.component';
import { ContentFileComponent } from '../content/content-file.component';
import {ContentFolderComponent} from "../content/content-folder.component";
import { EntityClickDirective } from './entity-click.directive';
import { RightClickComponent } from './right-click/right-click.component';
import { ContextMenuComponent } from './right-click/context-menu.component';
import { RenameMenuComponent } from './right-click/rename-menu.component';
import {RightClickService} from "./right-click/right-click.service";

@NgModule({
  declarations: [
    AppComponent,
    TreeFolder,
    TreeFile,
    ContentComponent,
    ContentFileComponent,
    ContentFolderComponent,
    EntityClickDirective,
    RightClickComponent,
    ContextMenuComponent,
    RenameMenuComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [MainService, RightClickService],
  bootstrap: [AppComponent]
})
export class AppModule { }
