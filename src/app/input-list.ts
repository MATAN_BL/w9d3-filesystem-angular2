import {LogicFolder} from "../LogicEntity/LogicFolder";
import {LogicFile} from "../LogicEntity/LogicFile";

export let inputList: any = new LogicFolder(0, 'root');
let id1 = new LogicFile(1, 'Abraham', 'Abraham_text');
inputList.addChild(id1);
let id2 = new LogicFile(2, 'David', 'David_text');
inputList.addChild(id2);
let id3 = new LogicFile(3, 'Sara', 'SaraText');
inputList.addChild(id3);
let id11 = new LogicFile(11, 'Benjamine', 'BennyText');
inputList.addChild(id11);
let id4 = new LogicFolder(4, 'dir1');
inputList.addChild(id4);
let id5 = new LogicFile(5, 'Aviv', 'Aviv file');
id4.addChild(id5);
let id6 = new LogicFolder(6, 'dir2');
id4.addChild(id6);
let id7 = new LogicFile(7, 'Gefen', 'Gefen File');
id6.addChild(id7);
let id8 = new LogicFolder(8,'dir3');
id6.addChild(id8);
let id9 = new LogicFile(9, 'Shlomi', 'shlomi file');
id8.addChild(id9);

export var rootId = 0;

//   {
//   name: 'root', id: 0, type: 'folder', children: [
//     {name: 'dir1', id: 1, type: 'folder', children: []},
//     {name: 'dir2', id: 2, type: 'folder', children:[
//       {name: 'David', id:4, type: 'file', content: "This is David File"}
//     ]},
//     {name: 'Michal', id: 3, type: 'file', content: "This is Michal's TreeFile"}
//   ]
// };
