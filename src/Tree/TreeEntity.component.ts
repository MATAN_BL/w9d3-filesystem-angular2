import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'entity',
  styleUrls: ['TreeEntity.styles.scss']
})
export abstract class TreeEntity {

  private _name: string;
  private _id: number;

  constructor(_name: string, _id: number) {
    this._name = _name;
    this._id = _id;
  }
  abstract getType();


  get name(): string {
    return this._name;
  }
  set name(value: string) {
    this._name = value;
  }
  get id(): number {
    return this._id;
  }
  set id(value: number) {
    this._id = value;
  }
}
