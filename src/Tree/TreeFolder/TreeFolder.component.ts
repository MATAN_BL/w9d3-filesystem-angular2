import {Component, OnInit, Input, forwardRef, ViewEncapsulation} from '@angular/core';
import {TreeEntity} from "../TreeEntity.component";
import {LogicFolder} from "../../LogicEntity/LogicFolder";
import {MainService} from "../../app/service";

@Component({
  selector: 'folder',
  templateUrl: 'TreeFolder.component.html',
  styleUrls: ['../TreeEntity.styles.css'],
  encapsulation: ViewEncapsulation.None
})
export class TreeFolder extends TreeEntity implements OnInit {

  @Input() logicFolder: LogicFolder;
  private expanded: boolean = false;

  constructor(private mainService: MainService) {
    super("", -1);
    this.expanded = false;
  }

  getType() {
    return "folder";
  }
  getSignOnButton() {
    var ret = (this.expanded ? '-' : '+');
    return ret;
  }
  changeExpandMode(){
    this.expanded = !this.expanded;
  }

  ngOnInit() {
    // this.name = this.logicFolder.name;
    // this.id = this.logicFolder.id;
  }
}
