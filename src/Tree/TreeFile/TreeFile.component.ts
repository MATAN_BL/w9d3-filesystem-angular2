import {Component, OnInit, Input, forwardRef, ViewEncapsulation} from '@angular/core';
import {TreeEntity} from "../TreeEntity.component";
import {LogicFile} from "../../LogicEntity/LogicFile";
import {MainService} from "../../app/service";

@Component({
  selector: 'file',
  templateUrl: 'TreeFile.component.html',
  styleUrls: ['../TreeEntity.styles.css' ],
  encapsulation: ViewEncapsulation.None
})
export class TreeFile extends TreeEntity {
  @Input() logicFile: LogicFile;

  constructor(private mainService: MainService) {
    super("", -1);
  }
  getType() {
    return "file";
  }
  ngOnInit() {
    this.name = this.logicFile.name;
    this.id = this.logicFile.id;
  }


}
