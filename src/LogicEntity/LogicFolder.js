"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var LogicEntity_1 = require("./LogicEntity");
var LogicFolder = (function (_super) {
    __extends(LogicFolder, _super);
    function LogicFolder(_id, _name) {
        _super.call(this, _name, _id);
        this._children = [];
    }
    LogicFolder.prototype.addChild = function (child) {
        this._children.push(child);
    };
    LogicFolder.prototype.getType = function () {
        return 'folder';
    };
    LogicFolder.prototype.deleteChild = function (id) {
        for (var i = 0; i < this.children.length; i++) {
            if (this.children[i].id == id) {
                this.children.splice(i, 1);
                return true;
            }
        }
        return false;
    };
    Object.defineProperty(LogicFolder.prototype, "children", {
        get: function () {
            return this._children;
        },
        set: function (value) {
            this._children = value;
        },
        enumerable: true,
        configurable: true
    });
    return LogicFolder;
}(LogicEntity_1.LogicEntity));
exports.LogicFolder = LogicFolder;
