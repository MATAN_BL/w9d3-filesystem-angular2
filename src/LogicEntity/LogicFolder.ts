import {LogicEntity} from "./LogicEntity";

export class LogicFolder extends LogicEntity {

  private _children: any;
  constructor(_id: number, _name: string) {
    super(_name, _id);
    this._children = [];
  }

  addChild(child: LogicEntity) {
    this._children.push(child);
  }

  getType() {
    return 'folder';
  }

  deleteChild(id: number) {
    for (let i = 0; i < this.children.length; i++) {
      if (this.children[i].id == id) {
        this.children.splice(i,1);
        return true
      }
    }
    return false;
  }

  get children(): any {
    return this._children;
  }

  set children(value: any) {
    this._children = value;
  }

// getFolderChildren() {
  //   let ret = [];
  //   for (let child of this.children) {
  //     if (child.getType() == 'folder') {
  //       ret.push(child);
  //     }
  //   }
  //   return ret;
  // }
  // getFileChildren() {
  //   let ret = [];
  //   for (let child of this.children) {
  //     if (child.getType() == 'file') {
  //       ret.push(child);
  //     }
  //   }
  //   return ret;
  // }

}
