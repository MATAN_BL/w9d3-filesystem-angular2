import {LogicEntity} from "./LogicEntity";
import {Input} from "@angular/core";

export class LogicFile extends LogicEntity {
  private _content: string;

  constructor(_id: number, _name: string, _content: string) {
    super(_name, _id);
    this._content = _content;
  }

  getType() {
    return "file";
  }

  get content(): string {
    return this._content;
  }

  set content(value: string) {
    this._content = value;
  }
}
