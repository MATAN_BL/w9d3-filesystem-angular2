"use strict";
var LogicEntity = (function () {
    function LogicEntity(_name, _id) {
        this._name = _name;
        this._id = _id;
        this.isBeingRenamed = false;
    }
    Object.defineProperty(LogicEntity.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogicEntity.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogicEntity.prototype, "isBeingRenamed", {
        get: function () {
            return this._isBeingRenamed;
        },
        set: function (value) {
            this._isBeingRenamed = value;
        },
        enumerable: true,
        configurable: true
    });
    return LogicEntity;
}());
exports.LogicEntity = LogicEntity;
