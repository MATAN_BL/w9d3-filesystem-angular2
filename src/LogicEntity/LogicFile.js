"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var LogicEntity_1 = require("./LogicEntity");
var LogicFile = (function (_super) {
    __extends(LogicFile, _super);
    function LogicFile(_id, _name, _content) {
        _super.call(this, _name, _id);
        this._content = _content;
    }
    LogicFile.prototype.getType = function () {
        return "file";
    };
    Object.defineProperty(LogicFile.prototype, "content", {
        get: function () {
            return this._content;
        },
        set: function (value) {
            this._content = value;
        },
        enumerable: true,
        configurable: true
    });
    return LogicFile;
}(LogicEntity_1.LogicEntity));
exports.LogicFile = LogicFile;
